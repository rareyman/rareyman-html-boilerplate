#!/bin/bash
# ------------------------------------------------------------------
# [Author] Title
#          Description
# ------------------------------------------------------------------

cp -rf bower_components/bootstrap/scss/ ./styles/scss/bootstrap-scss
cp -rf bower_components/bootstrap/dist/js/bootstrap.min.js ./scripts/bootstrap.min.js
cp -rf bower_components/html5-boilerplate/dist/js/* ./scripts/
cp -rf bower_components/html5-boilerplate/dist/css/* ./styles/css/
cp -rf bower_components/html5-boilerplate/dist/*.txt ./
cp -rf bower_components/html5-boilerplate/dist/*.html ./
cp -rf bower_components/html5-boilerplate/dist/*.xml ./
cp -rf bower_components/html5-boilerplate/dist/*.webmanifest ./
cp -rf bower_components/html5-boilerplate/dist/*.png ./styles/icons/
cp -rf bower_components/html5-boilerplate/dist/*.ico ./styles/icons/

echo "======================================================================"
echo "== Remember to adjust paths to bootstrap, jquery, icons, etc."
echo "== in index.html and 404.html!"
echo "======================================================================"